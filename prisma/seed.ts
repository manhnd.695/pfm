import { PrismaClient } from '@prisma/client';
import { hashSync } from 'bcryptjs';
const prisma = new PrismaClient();

async function main() {
  await prisma.permission.createMany({
    data: [
      { code: 'read:category' },
      { code: 'create:category' },
      { code: 'update:category' },
      { code: 'delete:category' },
      { code: 'read:role' },
      { code: 'update:role' },
    ],
  });

  const adminRole = await prisma.role.create({
    data: { code: 'ADMIN' },
  });

  const userRole = await prisma.role.create({
    data: { code: 'USER' },
  });

  const hashedPassword = hashSync('123456aA@', 12);

  await prisma.user.create({
    data: {
      email: 'manhnd.695@gmail.com',
      password: hashedPassword,
      name: 'Administrator',
      roles: {
        createMany: {
          data: [
            {
              roleId: adminRole.id,
            },
            {
              roleId: userRole.id,
            },
          ],
        },
      },
    },
  });

  const permissions = await prisma.permission.findMany();

  await prisma.rolePermission.createMany({
    data: permissions.map((permission) => ({
      roleId: adminRole.id,
      permissionId: permission.id,
    })),
  });

  const userPermissions = await prisma.permission.findMany({
    where: {
      code: {
        in: [
          'read:category',
          'create:category',
          'update:category',
          'delete:category',
        ],
      },
    },
  });

  await prisma.rolePermission.createMany({
    data: userPermissions.map((permission) => ({
      roleId: userRole.id,
      permissionId: permission.id,
    })),
  });
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
