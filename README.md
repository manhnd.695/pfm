# PFM

Personal Finance Management is a personal project of ManhND to manage his money which is how much money that his spend in a specific time and much more

## Getting started

First, we need to install all dependencies of the project, run:

```
npm install
```

## Docker

To install postgresql database, run:

```
docker compose -f ./docker/docker-compose.yml up -d
```

## Prisma

To generate database schema and seed necessary data, run:

```
npx prisma migrate reset
```

## Backend

To run backend:

```
nx serve api
```

## Frontend

To run frontend:

```
nx serve client
```
