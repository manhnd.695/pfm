import { SubmitHandler, useForm } from 'react-hook-form';

interface Credentials {
  email: string;
  password: string;
}

export function LoginPage() {
  const { register, handleSubmit } = useForm<Credentials>();

  const onSubmit: SubmitHandler<Credentials> = (data) => console.log(data);

  return (
    <div>
      <form onSubmit={handleSubmit(onSubmit)}>
        <label className="block">
          <span className="block text-sm font-medium text-slate-700">
            Email
          </span>
          <input
            type="email"
            className="peer"
            {...register('email', { required: true })}
          />
          <p className="mt-2 invisible peer-invalid:visible text-pink-600 text-sm">
            Please provide a valid email address.
          </p>
        </label>

        <label className="block">
          <span className="block text-sm font-medium text-slate-700">
            Password
          </span>
          <input
            type="password"
            className="peer"
            {...register('password', { required: true })}
          />
          <p className="mt-2 invisible peer-invalid:visible text-pink-600 text-sm">
            Please provide a valid email address.
          </p>
        </label>
      </form>
    </div>
  );
}
