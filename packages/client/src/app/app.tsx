import { Route, Routes, Link } from 'react-router-dom';
import './index.css';
import { LoginPage } from './pages/login.page';

export function App() {
  return <LoginPage />;
}

export default App;
