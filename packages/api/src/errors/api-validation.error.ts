import { ValidationError } from 'express-validator';

export class ApiValidationError extends Error {
  constructor(public errors: ValidationError[]) {
    super('Validation Error');
  }
}
