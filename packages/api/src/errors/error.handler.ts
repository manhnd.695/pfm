/* eslint-disable @typescript-eslint/no-unused-vars */
import { NextFunction, Request, Response } from 'express';
import { ApiError } from './api.error';
import { HttpStatusCode } from './http-status-code';
import { ApiValidationError } from './api-validation.error';
import { ValidationError } from 'express-validator';

export function handleNotFound(
  req: Request,
  res: Response,
  next: NextFunction
) {
  next(new ApiError('Not found', HttpStatusCode.NotFound));
}

export function errorHandler(
  error: Error,
  req: Request,
  res: Response,
  next: NextFunction
) {
  console.error(error);

  let message = error.message;
  let status: number;
  let errors: ValidationError[] | undefined;

  if (error instanceof ApiError) {
    status = error.status;
  } else if (error instanceof ApiValidationError) {
    status = HttpStatusCode.BadRequest;
    errors = error.errors;
  } else {
    status = HttpStatusCode.InternalServerError;
    message = 'Internal Server Error';
  }

  return res.status(status).json({
    timestamp: new Date(),
    path: req.path,
    status: status,
    message: message,
    errors: errors,
  });
}
