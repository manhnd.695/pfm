import express from 'express';
import * as swaggerUi from 'swagger-ui-express';
import swaggerJsdoc from 'swagger-jsdoc';
import * as path from 'path';
import * as bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import morgan from 'morgan';
import { errorHandler, handleNotFound } from './errors/error.handler';

import { authRouter } from './auth/auth.router';
import { roleRouter } from './roles/role.router';
import { categoryRouter } from './categories/category.router';

const port = process.env.PORT || 3333;

const openapiSpecification = swaggerJsdoc({
  swaggerDefinition: {
    openapi: '3.0.0',
    info: {
      title: 'PFM',
      description: 'Personal Finance Management',
      version: '0.0.0',
    },
    basePath: '/',
  },
  apis: ['**/*.router.ts', '**/*.controller.ts', '**/*.validator.ts'],
});

const app = express();

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(openapiSpecification));

app.use(morgan('combined'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/assets', express.static(path.join(__dirname, 'assets')));

app.use('/auth', authRouter);
app.use('/roles', roleRouter);
app.use('/categories', categoryRouter);

app.all('*', handleNotFound);
app.use(errorHandler);

const server = app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}`);
});

server.on('error', console.error);
