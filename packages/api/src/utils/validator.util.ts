import express from 'express';
import { ValidationChainLike } from 'express-validator/src/chain';
import { RunnableValidationChains } from 'express-validator/src/middlewares/schema';
import { ApiValidationError } from '../errors/api-validation.error';

function validate<T extends ValidationChainLike>(
  schema: RunnableValidationChains<T>
) {
  return async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) => {
    const result = await schema.run(req);
    const errors = result.flatMap((v) => v.array());

    if (!errors.length) {
      return next();
    }

    next(new ApiValidationError(errors));
  };
}

export { validate };
