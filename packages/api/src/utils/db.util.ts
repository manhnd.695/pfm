import { PrismaClient } from '@prisma/client';

declare global {
  // eslint-disable-next-line no-var
  var __db: PrismaClient | undefined;
}

if (!globalThis.__db) {
  global.__db = new PrismaClient({
    log: ['query', 'info', 'warn', 'error'],
  });
}

const db = global.__db;

export { db };
