import * as bcrypt from 'bcryptjs';

const SALT_ROUNDS = 12;

export async function hash(password: string) {
  const salt = await bcrypt.genSalt(SALT_ROUNDS);
  return bcrypt.hash(password, salt);
}

export async function compare(password: string, hash: string) {
  return bcrypt.compare(password, hash);
}
