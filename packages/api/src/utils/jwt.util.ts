import { User } from '@prisma/client';
import * as jwt from 'jsonwebtoken';

const SECRET_KEY = process.env.JWT_SECRET_KEY;

function sign(user: User, exprirationTime: number) {
  return jwt.sign({ id: user.id }, SECRET_KEY, {
    expiresIn: exprirationTime,
  });
}

function verify(token: string) {
  const result = jwt.verify(token, SECRET_KEY);

  if (typeof result === 'string') {
    throw new Error('Bad JWT');
  }

  return result;
}

export { verify, sign };
