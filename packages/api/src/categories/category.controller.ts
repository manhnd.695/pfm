import { catchAsync } from '../utils/async.util';

const getCategories = catchAsync(async (req, res) => {
  return res.json();
});

const getCategory = catchAsync(async (req, res) => {
  return res.json();
});

const createCategory = catchAsync(async (req, res) => {
  return res.json();
});

const updateCategory = catchAsync(async (req, res) => {
  return res.json();
});

const deleteCategory = catchAsync(async (req, res) => {
  return res.json();
});

export {
  getCategories,
  getCategory,
  createCategory,
  updateCategory,
  deleteCategory,
};
