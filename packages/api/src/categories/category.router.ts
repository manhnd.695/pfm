import * as express from 'express';
import {
  createCategory,
  deleteCategory,
  getCategories,
  getCategory,
  updateCategory,
} from './category.controller';

const categoryRouter = express.Router();

categoryRouter.route('/').get(getCategories).post(createCategory);

categoryRouter
  .route('/{id}')
  .get(getCategory)
  .put(updateCategory)
  .delete(deleteCategory);

export { categoryRouter };
