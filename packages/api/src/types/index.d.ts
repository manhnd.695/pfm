declare namespace Express {
  interface Request {
    authorization: User;
  }
}
