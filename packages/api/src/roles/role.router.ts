import * as express from 'express';
import { getRoles } from './role.controller';
import { authorization } from '../auth/auth.middleware';

const roleRouter = express.Router();

roleRouter.route('/').get(authorization, getRoles);

export { roleRouter };
