import { catchAsync } from '../utils/async.util';

const getRoles = catchAsync(async (req, res) => {
  return res.json({ message: 'Get roles' });
});

export { getRoles };
