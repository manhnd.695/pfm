import { db } from '../utils/db.util';

export async function findByEmail(email: string) {
  return db.user.findUnique({
    where: {
      email: email,
    },
  });
}

export async function findByUserId(id: number) {
  return db.user.findUnique({
    where: {
      id: id,
    },
    select: {
      name: true,
      email: true,
      id: true,
      roles: {
        select: {
          role: {
            select: {
              code: true,
              permissions: {
                select: {
                  permission: {
                    select: {
                      code: true,
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
  });
}
