import { ApiError } from '../errors/api.error';
import { HttpStatusCode } from '../errors/http-status-code';
import { findByUserId } from '../users/user.service';
import { catchAsync } from '../utils/async.util';
import { verify } from '../utils/jwt.util';

const authorization = catchAsync(async (req, res, next) => {
  const token = req.cookies.access_token;

  if (!token) {
    return next(new ApiError('Forbidden', HttpStatusCode.Forbidden));
  }

  try {
    const data = verify(token);
    const user = await findByUserId(data.id);
    req.authorization = user;

    next();
  } catch (e) {
    return next(new ApiError('Forbidden', HttpStatusCode.Forbidden));
  }
});

export { authorization };
