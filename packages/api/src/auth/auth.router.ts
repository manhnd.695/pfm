import * as express from 'express';
import { getAuthorization, login, logout, signup } from './auth.controller';
import { validate } from '../utils/validator.util';
import { loginSchema, signupSchema } from './auth.validator';
import { authorization } from './auth.middleware';

const authRouter = express.Router();

/**
 *
 * @openapi
 *
 * /auth/login:
 *   post:
 *     tags:
 *       - Auth
 *     summary: Logs in and returns the authentication cookie
 *     requestBody:
 *       required: true
 *       description: A JSON object containing the email and password
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/LoginRequest'
 *     responses:
 *       '200':
 *         description: >
 *           Successfully authenticated.
 *           The token is returned in a cookie named `access_token`. You need to include this cookie in subsequent requests.
 *         headers:
 *           Set-Cookie:
 *            schema:
 *              type: string
 *              example: access_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NCwiaWF0IjoxNjgzNDYyOTAwLCJleHAiOjI1NDc0NjI5MDB9.uoyqicYx0adkH2KSJReVZjLhYr-GLkr1U26mPUL53FY; Max-Age=864000; Path=/; Expires=Wed, 17 May 2023 12:35:00 GMT; HttpOnly
 *       '400':
 *         description: Bad request.
 *       '5XX':
 *         description: Unexpected error.
 */
authRouter.post('/login', validate(loginSchema), login);

authRouter.post('/info', authorization, getAuthorization);

/**
 *
 * @openapi
 *
 * /auth/signup:
 *   post:
 *     tags:
 *       - Auth
 *     summary: Register a user
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/SignupRequest'
 *     responses:
 *       '201':
 *         description: Created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/SignupResponse'
 *       '400':
 *         description: Bad request.
 *       '5XX':
 *         description: Unexpected error.
 */
authRouter.post('/signup', validate(signupSchema), signup);

authRouter.post('/logout', authorization, logout);

export { authRouter };
