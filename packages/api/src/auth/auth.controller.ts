import { HttpStatusCode } from '../errors/http-status-code';
import { catchAsync } from '../utils/async.util';
import { generateToken, register } from './auth.service';

const login = catchAsync(async (req, res) => {
  const { token, expirationTime } = await generateToken(
    req.body.email,
    req.body.password
  );

  return res
    .status(200)
    .cookie('access_token', token, {
      httpOnly: true,
      secure: process.env.NODE_ENV === 'production',
      maxAge: expirationTime,
    })
    .json({
      message: 'Successfully logged in',
    });
});

/**
 * @openapi
 *
 * components:
 *   schemas:
 *     SignupResponse:
 *       type: object
 *       properties:
 *         id:
 *           type: integer
 *           default: 1
 *         name:
 *           type: string
 *           default: John De
 *         email:
 *           type: string
 *           default: john.de@example.com
 */
const signup = catchAsync(async (req, res) => {
  const user = req.body;

  const createdUser = await register(user);

  return res.status(HttpStatusCode.Created).json({
    ...createdUser,
    password: undefined,
  });
});

export const getAuthorization = catchAsync(async (req, res) => {
  return res.status(200).json({
    ...req.authorization,
    password: undefined,
  });
});

const logout = catchAsync(async (req, res) => {
  return res
    .clearCookie('access_token')
    .status(200)
    .json({ message: 'Successfully logged out!' });
});

export { login, signup, logout };
