import { ExpressValidator } from 'express-validator';
import { findByEmail } from '../users/user.service';

const authExpressValidator = new ExpressValidator({
  isEmailNotInUse: async (value: string) => {
    const user = await findByEmail(value);

    if (user) {
      throw new Error(`Email ${value} is already in use`);
    }
  },
});

const { checkSchema } = authExpressValidator;

/**
 * @openapi
 *
 * components:
 *   schemas:
 *     SignupRequest:
 *       type: object
 *       required:
 *         - name
 *         - email
 *         - password
 *       properties:
 *         name:
 *           type: string
 *           default: John De
 *         email:
 *           type: string
 *           default: john.de@example.com
 *         password:
 *           type: string
 *           default: 123456aA@
 */
export const signupSchema = checkSchema({
  name: {
    trim: true,
    notEmpty: {
      errorMessage: 'Name must not be empty',
      bail: true,
    },
    isLength: {
      options: { min: 2, max: 32 },
      errorMessage: 'Name must have 2 to 32 characters',
    },
  },
  email: {
    trim: true,
    notEmpty: {
      errorMessage: 'Email must not be empty',
      bail: true,
    },
    isEmail: {
      errorMessage: 'Email is invalid',
      bail: true,
    },
    isEmailNotInUse: true,
  },
  password: {
    trim: true,
    isLength: {
      options: { min: 3, max: 255 },
      errorMessage: 'Password must have 3 to 255 characters',
    },
  },
});

/**
 * @openapi
 *
 * components:
 *   schemas:
 *     LoginRequest:
 *       type: object
 *       required:
 *         - email
 *         - password
 *       properties:
 *         email:
 *           type: string
 *           default: john.de@example.com
 *         password:
 *           type: string
 *           default: 123456aA@
 */
export const loginSchema = checkSchema({
  email: {
    trim: true,
    notEmpty: {
      errorMessage: 'Email must not be empty',
      bail: true,
    },
    isEmail: {
      errorMessage: 'Email is invalid',
    },
  },
  password: {
    trim: true,
    notEmpty: {
      errorMessage: 'Password must not be empty',
    },
  },
});
