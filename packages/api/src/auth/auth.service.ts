import { ApiError } from '../errors/api.error';
import { HttpStatusCode } from '../errors/http-status-code';
import { findByEmail } from '../users/user.service';
import { db } from '../utils/db.util';
import { sign } from '../utils/jwt.util';
import { compare, hash } from '../utils/security.util';

export async function register(user) {
  const hashedPassword = await hash(user.password);

  const userRole = await db.role.findUnique({
    where: {
      code: 'USER',
    },
  });

  return db.user.create({
    data: {
      name: user.name,
      email: user.email,
      password: hashedPassword,
      roles: {
        create: [
          {
            roleId: userRole.id,
          },
        ],
      },
    },
    include: {
      roles: {
        select: {
          role: {
            select: {
              code: true,
              permissions: {
                select: {
                  permission: {
                    select: {
                      code: true,
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
  });
}

export async function generateToken(email: string, password: string) {
  const user = await findByEmail(email);

  if (!user) {
    throw new ApiError(
      'Email or password is incorrect',
      HttpStatusCode.BadRequest
    );
  }

  const isMatched = await compare(password, user.password);

  if (!isMatched) {
    throw new ApiError(
      'Email or password is incorrect',
      HttpStatusCode.BadRequest
    );
  }

  const expirationTime = +process.env.JWT_EXPIRATION_TIME;

  const token = sign(user, expirationTime);

  return {
    token,
    expirationTime,
  };
}
